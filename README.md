
# Online Shop

An Online Shop for selling your products Creats with Asp.net Core.


## Tech Stack

**Client:** Html, Css, Bootstrap

**Server:** Asp.net core, Ef core, Razor Pages, Sql Server

## Installation
 
 In this project you need to install sql server.
 
 Then you need to run the following commands in Package Manager Consloe.
```bash
  Add-Migration Init-Database
```

```bash
  Update-Database
```


## Features

- Zarinpal Test Payment Gateway


## Site Admin User Name and Password

- Email : admin@gmail.com
- Password : admin123

## IDE USED
- Visual Studio 2022
## Developer

- [@Parsa Dadashi](https://github.com/ParsaDdshi)

