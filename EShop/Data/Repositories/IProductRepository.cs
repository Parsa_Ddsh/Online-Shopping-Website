﻿using EShop.Models;
using System.Collections;
using System.Collections.Generic;

namespace EShop.Data.Repositories
{
    public interface IProductRepository
    {
        IEnumerable<Product> GetAllProducts();
        Product GetProductIncludeItem(int id);
        List<Category> GetProductCategories(int id);
        List<Product> GetCategoryProducts(int id);
        List<Category> GetCategories();
        void InsertItem(Item item);
        void InsertProduct(Product product);
        void Save();
        void InsertCategoryToProduct(CategoryToProduct categoryToProduct);
        Product GetProductById(int id);
        Item GetItemById(int id);
        void DeleteProduct(Product product);
        void DeleteItem(Item item);
        ProductViewModel GetProduct(int id);
        List<int> GetProductGroups(int id);
        void DeleteCategoryToProducts(int id);
        void EditCategoryToProducts(List<int> selectedGroups, int productId);
        IEnumerable<Category> GetAllCategories();
        IEnumerable<ShowProductViewModel> GetCategoryForSidebar();
        void InsertCategory(Category category);
        Category GetCategoryById(int id);
        void UpdateCategory(Category category);
        bool IsCategoryExists(int id);
        void RemoveCategoryRelations(int categoryId);
        void RemoveCategory(Category category);
    }
}
