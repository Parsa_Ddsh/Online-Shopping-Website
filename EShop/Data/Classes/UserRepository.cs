﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using EShop.Data.Repositories;
using EShop.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace EShop.Data.Classes
{
    public class UserRepository : IUserRepository
    {
        private readonly EShopContext _context;

        public UserRepository(EShopContext context)
        {
            _context = context;
        }
        public void AddUser(User user)
        {
            _context.Users.Add(user);
        }

        public bool IsExistByEmail(string email)
        {
            return _context.Users.Any(u => u.Email == email);
        }

        public User GetUserForLogin(string email, string password)
        {
            return _context.Users.SingleOrDefault(u => u.Email == email && u.Password == password);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public User GetUserById(int id)
        {
            return _context.Users.FirstOrDefault(m => m.UserId == id);
        }

        public void RemoveUser(User user)
        {
            _context.Users.Remove(user);
        }

        public void UpdateUser(User user)
        {
            _context.Attach(user).State = EntityState.Modified;
        }

        public bool IsUserExistById(int id)
        {
            return _context.Users.Any(e => e.UserId == id);
        }

        public List<User> GetAllUsers()
        {
            return _context.Users.ToList();
        }
    }
}
