﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using EShop.Data;
using EShop.Models;
using EShop.Data.Classes;
using EShop.Data.Repositories;

namespace EShop.Pages.Admin.ManageUsers
{
    public class DeleteModel : PageModel
    {
        private readonly IUserRepository _userRepository;
        public DeleteModel(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [BindProperty]
        public new User User { get; set; }

        public IActionResult OnGet(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            User = _userRepository.GetUserById(id.Value);

            if (User == null)
            {
                return NotFound();
            }
            return Page();
        }

        public IActionResult OnPost(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            User = _userRepository.GetUserById(id.Value);

            if (User != null)
            {
                _userRepository.RemoveUser(User);
                _userRepository.Save();
            }

            return RedirectToPage("./Index");
        }
    }
}
