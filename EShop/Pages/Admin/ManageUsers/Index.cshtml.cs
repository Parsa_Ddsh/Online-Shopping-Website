﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using EShop.Data;
using EShop.Models;
using EShop.Data.Repositories;

namespace EShop.Pages.Admin.ManageUsers
{
    public class IndexModel : PageModel
    {
        private readonly IUserRepository _userRepository;
        public IndexModel(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public new IList<User> User { get;set; }

        public void OnGet()
        {
            User = _userRepository.GetAllUsers();
        }
    }
}
