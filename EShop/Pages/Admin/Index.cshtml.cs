using System.Collections.Generic;
using EShop.Data;
using EShop.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace EShop.Pages.Admin
{
    public class IndexModel : PageModel
    {
        private EShopContext _context;

        public IndexModel(EShopContext context)
        {
            _context = context;
        }
        public IEnumerable<Product> Products { get; set; }
        public void OnGet()
        {
            Products = _context.Products
                .Include(p => p.Item);
        }

        public void OnPost()
        {

        }
    }
}
