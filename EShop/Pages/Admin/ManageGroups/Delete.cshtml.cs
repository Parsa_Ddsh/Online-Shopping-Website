﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using EShop.Models;
using EShop.Data.Repositories;

namespace EShop.Pages.Admin.ManageGroups
{
    public class DeleteModel : PageModel
    {
        private readonly IProductRepository _productRepository;

        public DeleteModel(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        [BindProperty]
        public Category Category { get; set; }

        public IActionResult OnGet(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Category = _productRepository.GetCategoryById(id.Value);

            if (Category == null)
            {
                return NotFound();
            }
            return Page();
        }

        public IActionResult OnPost(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Category = _productRepository.GetCategoryById(id.Value);

            if (Category != null)
            {
                _productRepository.RemoveCategoryRelations(id.Value);
                _productRepository.RemoveCategory(Category);
                _productRepository.Save();
            }

            return RedirectToPage("./Index");
        }
    }
}
