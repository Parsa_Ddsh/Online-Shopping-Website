using System.IO;
using System.Linq;
using EShop.Data;
using EShop.Data.Repositories;
using EShop.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EShop.Pages.Admin
{
    public class DeleteModel : PageModel
    {
        private IProductRepository _productRepository;
        public DeleteModel(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        [BindProperty]
        public Product Product { get; set; }
        public void OnGet(int id)
        {
            Product = _productRepository.GetProductById(id);
        }

        public IActionResult OnPost()
        {
            var product = _productRepository.GetProductById(Product.Id);
            var item = _productRepository.GetItemById(Product.Id);

            _productRepository.DeleteItem(item);
            _productRepository.DeleteProduct(product);
            _productRepository.Save();

            string filePath = Path.Combine(Directory.GetCurrentDirectory(),
                "wwwroot",
                "images",
                product.Id + "jpg");

            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            return RedirectToPage("Index");
        }
    }
}
