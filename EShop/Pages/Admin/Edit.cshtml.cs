using System.Collections.Generic;
using System.IO;
using System.Linq;
using EShop.Data;
using EShop.Data.Repositories;
using EShop.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace EShop.Pages.Admin
{
    public class EditModel : PageModel
    {
        private readonly IProductRepository _productRepository;
        public EditModel(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        [BindProperty]
        public ProductViewModel Product { get; set; }
        [BindProperty]
        public List<int> SelectedGroups { get; set; }
        public List<int> ProductGroups { get; set; }

        public void OnGet(int id)
        {
            var product = _productRepository.GetProduct(id);
            Product = product;

            product.Categories = _productRepository.GetCategories();
            ProductGroups = _productRepository.GetProductGroups(id);
        }

        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
                return Page();

            var product = _productRepository.GetProductById(Product.Id);
            var item = _productRepository.GetItemById(Product.Id);

            product.Name = Product.Name;
            product.Description = Product.Description;
            item.Price = Product.Price;
            item.QuantityInStock = Product.QuantityInStock;

            _productRepository.Save();

            if (Product.Picture?.Length > 0)
            {
                string filePath = Path.Combine(Directory.GetCurrentDirectory(),
                    "wwwroot",
                    "images",
                    product.Id + Path.GetExtension(Product.Picture.FileName));
                using var stream = new FileStream(filePath, FileMode.Create);
                Product.Picture.CopyTo(stream);
            }
            _productRepository.DeleteCategoryToProducts(Product.Id);
            _productRepository.EditCategoryToProducts(SelectedGroups, Product.Id);

            return RedirectToPage("Index");
        }
    }
}
