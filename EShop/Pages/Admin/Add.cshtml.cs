using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using EShop.Data;
using EShop.Data.Repositories;
using EShop.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EShop.Pages.Admin
{
    public class AddModel : PageModel
    {
        private readonly IProductRepository _productRepository;
        public AddModel(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        [BindProperty]
        public ProductViewModel Product { get; set; }
        [BindProperty]
        public List<int> selectedGroups { get; set; }
        public void OnGet()
        {
            Product = new ProductViewModel()
            {
                Categories = _productRepository.GetCategories()
            };
        }

        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
                return Page();

            var item = new Item()
            {
                Price = Product.Price,
                QuantityInStock = Product.QuantityInStock
            };
            _productRepository.InsertItem(item);
            _productRepository.Save();

            var product = new Product()
            {
                Name = Product.Name,
                Description = Product.Description,
                Item = item
            };
            _productRepository.InsertProduct(product);
            _productRepository.Save();

            product.ItemId = product.Id;
            _productRepository.Save();

            if (Product.Picture?.Length > 0)
            {
                string filePath = Path.Combine(Directory.GetCurrentDirectory(),
                    "wwwroot",
                    "images",
                    product.Id + Path.GetExtension(Product.Picture.FileName));
                using (var stream = new FileStream(filePath,FileMode.Create))
                {
                    Product.Picture.CopyTo(stream);
                }
            }

            if(selectedGroups.Any() && selectedGroups.Count > 0)
            {
                foreach(var gr in selectedGroups)
                {
                    _productRepository.InsertCategoryToProduct(new CategoryToProduct()
                    {
                        CategoryId = gr,
                        ProductId = product.Id,
                    }); ;
                }
                _productRepository.Save();
            }
            return RedirectToPage("Index");
        }
    }
}
