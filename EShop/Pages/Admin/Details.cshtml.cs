using EShop.Data.Classes;
using EShop.Data.Repositories;
using EShop.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;

namespace EShop.Pages.Admin
{
    public class DetailsModel : PageModel
    {
        private readonly IProductRepository _productRepository;
        public DetailsModel(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public ProductViewModel Product { get; set; }
        public List<int> ProductGroups { get; set; }
        public IActionResult OnGet(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = _productRepository.GetProduct(id.Value);
            Product = product;

            product.Categories = _productRepository.GetCategories();
            ProductGroups = _productRepository.GetProductGroups(id.Value);

            if (User == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
