﻿using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.AspNetCore.Http;

namespace EShop.Models
{
    public class ProductViewModel
    {
        public int Id { get; set; }
        [DisplayName("نام")]
        public string Name { get; set; }
        [DisplayName("قیمت")]
        public decimal Price { get; set; }
        [DisplayName("توضیحات")]
        public string Description { get; set; }
        [DisplayName("تعداد کالا در انبار")]
        public int QuantityInStock { get; set; }
        [DisplayName("تصویر")]
        public IFormFile Picture { get; set; }
        [DisplayName("گروه های محصول")]
        public List<Category> Categories { get; set; }
    }
}
