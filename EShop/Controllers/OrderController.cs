﻿using EShop.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System;
using EShop.Data.Repositories;
using System.Linq;
using ZarinpalSandbox;

namespace EShop.Controllers
{
    public class OrderController : Controller
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IProductRepository _productRepository;
        public OrderController(IOrderRepository orderRepository,
            IProductRepository productRepository)
        {
            _orderRepository = orderRepository;
            _productRepository = productRepository;
        }

        [Authorize]
        public IActionResult AddToCart(int itemId)
        {
            var product = _productRepository.GetProductIncludeItem(itemId);
            if (product != null)
            {
                var userId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier).ToString());
                var order = _orderRepository.GetOrderByUserId(userId);

                if (order != null)
                {
                    var orderDetail = _orderRepository.GetOrderDetailByProductId(itemId, order.OrderId);
                    if (orderDetail != null)
                    {
                        orderDetail.Count += 1;
                    }
                    else
                    {
                        _orderRepository.InsertOrderDetail(new OrderDetail()
                        {
                            OrderId = order.OrderId,
                            Price = product.Item.Price,
                            ProductId = product.Id,
                            Count = 1
                        });
                    }
                }
                else
                {
                    order = new Order()
                    {
                        UserId = userId,
                        CreateDate = DateTime.Now,
                        IsOrderFinished = false,
                    };
                    _orderRepository.InsertOrder(order);
                    _orderRepository.Save();
                    _orderRepository.InsertOrderDetail(new OrderDetail()
                    {
                        OrderId = order.OrderId,
                        Price = product.Item.Price,
                        ProductId = product.Id,
                        Count = 1
                    });
                }
            }
            _orderRepository.Save();
            return RedirectToAction("ShowCart");
        }

        [Authorize]
        public IActionResult ShowCart()
        {
            var userId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier).ToString());
            var order = _orderRepository.GetOrderIncludeDetailAndProduct(userId);

            return View(order);
        }
        [Authorize]
        public IActionResult RemoveItemFromCart(int detailId)
        {
            var orderDetail = _orderRepository.GetOrderDetail(detailId);
            if (orderDetail.Count > 1)
            {
                orderDetail.Count -= 1;
            }
            else
            {
                _orderRepository.DeleteOrderDetail(orderDetail);
            }
            _orderRepository.Save();

            return RedirectToAction("ShowCart");
        }

        [Authorize]
        public IActionResult Payment()
        {
            int userId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
            var order = _orderRepository.GetOrderIncludeDetails(userId);
            if (order == null)
                return NotFound();

            var payment = new Payment((int)order.Details.Sum(d => d.Price));
            var res = payment.PaymentRequest($"پرداخت فاکتور شماره {order.OrderId}",
                "http://localhost:44373/Home/OnlinePayment/" + order.OrderId, User.Identity.Name, User.FindFirst("PhoneNumber").Value);
            if (res.Result.Status == 100)
            {
                return Redirect("https://sandbox.zarinpal.com/pg/StartPay/" + res.Result.Authority);
            }
            else
            {
                return BadRequest();
            }

        }

        public IActionResult OnlinePayment(int id)
        {
            if (HttpContext.Request.Query["Status"] != "" &&
                HttpContext.Request.Query["Status"].ToString().ToLower() == "ok" &&
                HttpContext.Request.Query["Authority"] != "")
            {
                string authority = HttpContext.Request.Query["Authority"].ToString();
                var order = _orderRepository.GetOrderByOrderId(id);
                var payment = new Payment((int)order.Details.Sum(d => d.Price));
                var res = payment.Verification(authority).Result;
                if (res.Status == 100)
                {
                    order.IsOrderFinished = true;
                    _orderRepository.UpdateOrder(order);
                    _orderRepository.Save();
                    ViewBag.code = res.RefId;
                    return View();
                }
            }

            return NotFound();
        }
    }
}
