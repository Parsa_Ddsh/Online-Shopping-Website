﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using EShop.Data.Repositories;
using EShop.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;

namespace EShop.Controllers
{
    public class AccountController : Controller
    {
        private IUserRepository _userRepository;

        public AccountController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        #region Register

        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Register(RegisterViewModel register)
        {
            if (!ModelState.IsValid)
                return View(register);

            var user = new User()
            {
                FullName = register.FullName,
                PhoneNumber = register.PhoneNumber,
                Email = register.Email.ToLower(),
                Password = register.Password,
                IsAdmin = false,
                RegisterTime = DateTime.Now
            };

            _userRepository.AddUser(user);
            _userRepository.Save();

            return View("SuccessRegister", register);

        }

        public IActionResult VerifyEmail(string email)
        {
            if (_userRepository.IsExistByEmail(email.ToLower()))
            {
                return Json("ایمیل وارد شده تکراری است");
            }
            return Json(true);
        }

        #endregion

        #region Login

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(LoginViewModel login)
        {
            if (!ModelState.IsValid)
                return View(login);

            var user = _userRepository.GetUserForLogin(login.Email.ToLower(), login.Password);

            if (user == null)
            {
                ModelState.AddModelError("Email", "اطلاعات وارد شده صحیح نمیباشد");
                return View(login);
            }

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.UserId.ToString()),
                new Claim(ClaimTypes.Name, user.Email),
                new Claim("IsAdmin", user.IsAdmin.ToString()),
                new Claim("FullName", user.FullName),
                new Claim("PhoneNumber", user.PhoneNumber),
                new Claim("FullName", user.FullName)
            };
            var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var principal = new ClaimsPrincipal(identity);
            var properties = new AuthenticationProperties
            {
                IsPersistent = login.RememberMe
            };
            HttpContext.SignInAsync(principal, properties);
            return Redirect("/");
        }
        #endregion
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return Redirect("/");
        }
    }
}
