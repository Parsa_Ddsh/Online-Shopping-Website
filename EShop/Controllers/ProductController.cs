﻿using System.Linq;
using EShop.Data;
using EShop.Data.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EShop.Controllers
{
    public class ProductController : Controller
    {
        EShopContext _context;
        IProductRepository _productRepository;

        public ProductController(IProductRepository productRepository, 
            EShopContext context)
        {
            _productRepository = productRepository;
            _context = context;
        }

        [Route("/Groups/{id}/{name}")]
        public IActionResult ShowProductsByGroupId(int id,  string name)
        {
            ViewData["GroupName"] = name;
            var products = _productRepository.GetCategoryProducts(id);
            return View(products);
        }
    }
}
