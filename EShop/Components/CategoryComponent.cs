﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EShop.Data;
using EShop.Data.Repositories;
using EShop.Models;
using Microsoft.AspNetCore.Mvc;

namespace EShop.Components
{
    public class CategoryComponent : ViewComponent
    {
        private IProductRepository _productRepository;

        public CategoryComponent(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var categories = _productRepository.GetCategoryForSidebar();
            return View("/Views/Components/CategoryComponent.cshtml",categories);
        }
    }
}
